package com.nemoeslovo.imagesearch.helper;

import android.util.Log;
import android.view.View;

/**
* Created by danilakolesnikov on 09/02/14.
*/
public class DefaultCallback {

    private View progressBar;
    private String imageLink;

    public DefaultCallback(View progressBar, String imageLink) {
        this.progressBar = progressBar;
        this.imageLink = imageLink;
    }

    public void onSuccess() {
        UIHelper.setViewVisibility(progressBar, false);
    }

    public void onError() {
        Log.d(this.getClass().getName(), "error when loading image" + imageLink);
    }

}
