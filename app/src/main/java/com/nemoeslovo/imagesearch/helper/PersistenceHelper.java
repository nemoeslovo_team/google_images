package com.nemoeslovo.imagesearch.helper;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.nemoeslovo.imagesearch.App;

/**
 * Created by danilakolesnikov on 09/02/14.
 */
public class PersistenceHelper {

    public static void save(String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String readString(String key, String defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        return preferences.getString(key, defaultValue);
    }

}
