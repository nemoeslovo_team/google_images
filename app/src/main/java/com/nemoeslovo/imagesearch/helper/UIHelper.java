package com.nemoeslovo.imagesearch.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.nemoeslovo.imagesearch.App;
import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.activity.fragment.ImagesPreviewsFragment;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class UIHelper {

    public static void loadImage(final String imageLink, ImageView image, final View progressBar, int size) {
        if (imageLink == null) return;
        int height = (int) (size * 0.7f);
        image.setMinimumWidth(size);
        image.setMinimumHeight(height);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);

        setViewVisibility(progressBar, false);
        App.getImageLibrary().load(imageLink, image, size, height);
    }

    public static void loadImage(final String imageLink, ImageView image, final View progressBar) {
        if (imageLink == null) return;
        setViewVisibility(progressBar, false);
        App.getImageLibrary().load(imageLink, image, 0, 0);
    }

    public static void setViewVisibility(View view, boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE
                                   : View.INVISIBLE;

        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    public static void hideKeyboard(Activity activity, EditText editText) {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static AlertDialog createNoConnectionAlert(final Activity context) {
        return new AlertDialog.Builder(context)
                .setTitle(R.string.no_connection)
                .setMessage(R.string.no_connection_message)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }


    public static AlertDialog createNoResultsDialog(Activity context, final EditText editText) {
        return new AlertDialog.Builder(context)
                .setTitle(R.string.nothing_found)
                .setMessage(R.string.nothing_found_detailed)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("Очистить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        editText.setText(ImagesPreviewsFragment.EMPTY_STRING);
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }
}
