package com.nemoeslovo.imagesearch.cache;

import com.nemoeslovo.imagesearch.helper.StreamUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by danilakolesnikov on 13/02/14.
 *
 * This simple implementation of disk lru cache requares
 * new empty exclusive directory for properly working.
 *
 * ISSUES:
 *   *** implement file filter to purpose of using any directory
 *
 *
 */

public class DiskCache {

    public static final String CACHE_FILE_SUFFIX = ".diskcache_entity";

    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 5, 60, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());


    private final File cacheRoot;
    private final long maxSize;
    private       long usedSpace;


    public DiskCache(File cacheRoot, long maxSize) {
        this.cacheRoot = cacheRoot;
        this.maxSize   = maxSize;
        usedSpace      = getUsedSpace();
    }

    public final synchronized void put(String key, byte[] value) throws IOException {
        final File         newFile      = getFile(key);
        final OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(newFile));
        outputStream.write(value);
        outputStream.close();
        usedSpace += value.length;
        trimToSize();
    }

    public final void put(File file, InputStream inputStream) throws IOException {
        OutputStream os = new FileOutputStream(file);
        StreamUtil.copyInputToOutputStream(inputStream, os);
        os.close();
    }

    public final synchronized byte[] get(String key) throws IOException {
        final File cachedFile = getFile(key);
        if (!cachedFile.exists()) {
            return null;
        }

        final InputStream inputStream = new BufferedInputStream(new FileInputStream(cachedFile));
        byte[] bytes = StreamUtil.convertStreamToByteArray(inputStream);
        inputStream.close();
        return bytes;
    }

    private String getFileName(String key) {
        return key.hashCode() + CACHE_FILE_SUFFIX;
    }

    public File getFile(String key) {
        return new File(cacheRoot, getFileName(key));
    }

    private synchronized void trimToSize() {

        executor.execute(new Runnable() {
            @Override
            public void run() {
                long sizeToTrim = getUsedSpace() - maxSize;

                if (sizeToTrim <= 0) {
                    return;
                }

                List<File> sortedFiles = Arrays.asList(cacheRoot.listFiles());
                Collections.sort(sortedFiles, new Comparator<File>() {
                    @Override
                    public int compare(File file, File file2) {
                        return Long.valueOf(file.lastModified()).compareTo(file2.lastModified());
                    }
                });

                long trimmed = 0;

                for (final File file: sortedFiles) {
                    if (file.delete()) {
                        trimmed += file.length();
                    }

                    if (trimmed > sizeToTrim) {
                        break;
                    }

                }
            }
        });
    }

    public boolean contains(String key) {
        return getFile(key).exists();
    }

    public synchronized boolean clear() {
        boolean result = true;

        for (final File cacheFile : cacheRoot.listFiles()) {
            if (!cacheFile.delete()) {
                result = false;
            }
        }
        usedSpace = 0;
        return result;
    }

    public long getUsedSpace() {
        long usage = 0;
        for (final File cacheFile : cacheRoot.listFiles()) {
            usage += cacheFile.length();
        }
        return usage;
    }

}
