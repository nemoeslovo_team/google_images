package com.nemoeslovo.imagesearch.network;

import com.nemoeslovo.imagesearch.network.CallbackWrapper;

import java.util.ArrayList;
import java.util.List;

/**
* Created by danilakolesnikov on 09/02/14.
*/
public class CallbacksManager {
    private List<CallbackWrapper> callbackWrappers = new ArrayList<CallbackWrapper>();

    public void addCallback(CallbackWrapper callbackWrapper) {
        callbackWrappers.add(callbackWrapper);
    }

    public void stopAllRequests() {
        for (CallbackWrapper callbackWrapper: callbackWrappers) {
            stopHandlingRequestCallback(callbackWrapper);
        }
        callbackWrappers.clear();
    }

    private void stopHandlingRequestCallback(CallbackWrapper callbackWrapper) {
        if (callbackWrapper != null) {
            callbackWrapper.setCancelled(true);
        }
    }
}
