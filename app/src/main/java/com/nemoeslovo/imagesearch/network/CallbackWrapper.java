package com.nemoeslovo.imagesearch.network;

import android.app.Activity;
import android.util.Log;

import com.nemoeslovo.imagesearch.business.request.Callback;
import com.nemoeslovo.imagesearch.business.request.IMappable;
import com.nemoeslovo.imagesearch.helper.UIHelper;

public abstract class CallbackWrapper<T extends IMappable> implements Callback<T> {

    private Activity activity;

    abstract public void doOnSuccess(T t);
    public void doOnFailure(){}

    private boolean cancell;
    public  boolean isCancell() { return cancell;}
    public  void    setCancelled(boolean cancel) { this.cancell = cancel; }

    public void success(T t) {
        if (!isCancell()) {
            doOnSuccess(t);
        }
    }

    public CallbackWrapper(Activity activity) {
        this.activity = activity;
    }

    public void failure(boolean isConnectionProblem, Exception exception) {
        if (isConnectionProblem) {
            UIHelper.createNoConnectionAlert(activity).show();
        }
        Log.e(this.getClass().getName(), "callback fauilure", exception);
    }
}
