package com.nemoeslovo.imagesearch.network;

import com.nemoeslovo.imagesearch.business.NetworkTask;
import com.nemoeslovo.imagesearch.business.request.IRequestFactory;
import com.nemoeslovo.imagesearch.business.request.base.Request;
import com.nemoeslovo.imagesearch.model.Result;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class RequestManager {

    public static final int    COUNT_PER_PAGE = 8;
    public static final String API_VERSION    = "1.0";

    private IRequestFactory  requestFactory;

    private CallbacksManager callbacksManager = new CallbacksManager();

    public RequestManager(IRequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public void getImages(String query, int start, CallbackWrapper<Result> callbackWrapper) {
        Request request = requestFactory.getImageSearchRequest(start, query);
        NetworkTask<Result> task = new NetworkTask<Result>(request, callbackWrapper, Result.class);
        task.execute();
        callbacksManager.addCallback(callbackWrapper);
    }

    public void stopAllRequests() {
        callbacksManager.stopAllRequests();
    }

}
