package com.nemoeslovo.imagesearch.model;

import com.nemoeslovo.imagesearch.business.request.IMappable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class ResponseData implements IMappable{

    public static final String RESULTS_KEY = "results";

    private List<Image> results;

    public List<Image> getImages() {
        return results;
    }

    //TODO: optimise mapping process
    @Override
    public void mapWithJSONObject(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = jsonObject.getJSONArray(RESULTS_KEY);
        if (jsonArray != null) {
            results = new ArrayList<Image>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                Image      image     = new Image();
                JSONObject imageJson = jsonArray.getJSONObject(i);
                image.mapWithJSONObject(imageJson);
                results.add(image);
            }
        }
    }
}
