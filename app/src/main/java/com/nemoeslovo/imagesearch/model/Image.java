package com.nemoeslovo.imagesearch.model;

import com.nemoeslovo.imagesearch.business.request.IMappable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class Image implements IMappable {

    public static final String URL_KEY = "url";
    private String url;

    public String getUrl() {
        return url;
    }

    @Override
    public void mapWithJSONObject(JSONObject jsonObject) throws JSONException {
        url = jsonObject.getString(URL_KEY);
    }
}
