package com.nemoeslovo.imagesearch.model;

import com.nemoeslovo.imagesearch.business.request.IMappable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class Result implements IMappable{

    public static final String RESPONSE_DATA_KEY = "responseData";

    private ResponseData responseData;
    private Boolean      images;

    public ResponseData getResponseData() {
        return responseData;
    }

    public List<Image> getImages() {
        if (responseData != null) {
            return responseData.getImages();
        }

        return null;
    }

    public boolean isResultEmpty() {
        return getImages() == null || getImages().size() == 0;
    }

    @Override
    public void mapWithJSONObject(JSONObject jsonObject) throws JSONException {
        responseData = new ResponseData();
        JSONObject   responseDataJson = jsonObject.getJSONObject(RESPONSE_DATA_KEY);
        responseData.mapWithJSONObject(responseDataJson);
    }
}
