package com.nemoeslovo.imagesearch.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nemoeslovo.imagesearch.App;
import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.activity.CustomScrollListener;
import com.nemoeslovo.imagesearch.activity.adapter.ImagesAdapter;
import com.nemoeslovo.imagesearch.network.CallbackWrapper;
import com.nemoeslovo.imagesearch.network.RequestManager;
import com.nemoeslovo.imagesearch.helper.PersistenceHelper;
import com.nemoeslovo.imagesearch.helper.UIHelper;
import com.nemoeslovo.imagesearch.model.Image;
import com.nemoeslovo.imagesearch.model.Result;

/**
* Created by danilakolesnikov on 07/02/14.
*/
public class ImagesPreviewsFragment extends Fragment {

    public static final String  EMPTY_STRING        = "";
    public static final String  FULL_IMAGE_URL_TAG  = "full_image_url";
    public static final String  LAST_QUERY_KEY      = "last_query";

    private GridView            imageGrid;
    private Button              searchButton;
    private EditText            searchQueryField;
    private ProgressBar         progressBar;
    private ArrayAdapter<Image> imagesAdapter;
    private RequestManager      requestManager = new RequestManager(App.getRequestFactory());

    public ImagesPreviewsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        if (savedInstanceState == null) {
            imageGrid        = (GridView) rootView.findViewById(R.id.images_grid);
            searchButton     = (Button)   rootView.findViewById(R.id.search_button);
            searchQueryField = (EditText) rootView.findViewById(R.id.search_query_field);
            progressBar      = (ProgressBar) rootView.findViewById(R.id.progressBar);

            searchButton.setOnClickListener(onSearchClickListener);

            if (imagesAdapter == null) {
                imagesAdapter = new ImagesAdapter(getActivity());
            }
            imageGrid.setAdapter(imagesAdapter);
            imageGrid.setOnItemClickListener(onPreviewImageClickListener);

            //pagination
            CustomScrollListener onScrollListener = new CustomScrollListener();
            onScrollListener.setScrollEndlistener(scrollEndlistener);
            imageGrid.setOnScrollListener(onScrollListener);

            //restore last query
            String lastQuery = PersistenceHelper.readString(LAST_QUERY_KEY, EMPTY_STRING);
            searchQueryField.setText(lastQuery);
            searchQueryField.setSelection(lastQuery.length());

            //handy search on keyboard
            searchQueryField.setOnEditorActionListener(onEditorActionListener);
        }

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        requestManager.stopAllRequests();
    }

    //=================== listeners =======================//

    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch(getQueryText());
                return true;
            }
            return false;
        }
    };

    private View.OnClickListener onSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           performSearch(getQueryText());
        }
    };

    private CustomScrollListener.ScrollEndListener scrollEndlistener = new CustomScrollListener.ScrollEndListener() {
        @Override
        public void scrollEndReached() {
            if (!endInPaginationReached) {
                requestImages(getQueryText(), imagesAdapter.getCount());
            }
        }
    };

    private AdapterView.OnItemClickListener onPreviewImageClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            Intent intent = new Intent(getActivity(), FullscreenImageActivity.class);
            intent.putExtra(FULL_IMAGE_URL_TAG, imagesAdapter.getItem(position).getUrl());
            getActivity().startActivity(intent);
        }
    };

    //============== Private methods ================//
    private void requestImages(String query, int offset) {
        UIHelper.setViewVisibility(progressBar, true);
        requestManager.getImages(query, offset, new CallbackWrapper<Result>(getActivity()) {
            @Override
            public void doOnSuccess(Result result) {
                processImageSearchResult(result);
            }
        });
    }

    private String getQueryText() {
        if (searchQueryField != null && searchQueryField.getText() != null) {
            return searchQueryField.getText().toString();
        }
        return EMPTY_STRING;
    }

    private void processImageSearchResult(Result result) {
        UIHelper.setViewVisibility(progressBar, false);

        if (!handleNoResults(result)) {
            for (Image image : result.getImages()) {
                imagesAdapter.add(image);
            }
        }
    }

    boolean endInPaginationReached = false;
    private boolean handleNoResults(Result result) {
        if (result.isResultEmpty()) {
            if (imagesAdapter.getCount() == 0) {
                UIHelper.createNoResultsDialog(getActivity(), searchQueryField).show();
            }
            endInPaginationReached = true;
            return true;
        }
        return false;
    }

    private void performSearch(String queryText) {
        endInPaginationReached = false;
        UIHelper.hideKeyboard(getActivity(), searchQueryField);
        PersistenceHelper.save(LAST_QUERY_KEY, queryText);
        imagesAdapter.clear();
        requestImages(getQueryText(), 0);
    }

}
