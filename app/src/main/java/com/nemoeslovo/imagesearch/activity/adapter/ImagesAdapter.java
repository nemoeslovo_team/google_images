package com.nemoeslovo.imagesearch.activity.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.helper.UIHelper;
import com.nemoeslovo.imagesearch.model.Image;

import java.util.ArrayList;

/**
* Created by danilakolesnikov on 07/02/14.
*/
public class ImagesAdapter extends ArrayAdapter<Image> {

    public ImagesAdapter(Context context) {
        super(context, 0, new ArrayList<Image>());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.image_view, null);
            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }

        Holder holder   = (Holder) convertView.getTag();
        String imageUrl = getItem(position).getUrl();

        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        UIHelper.loadImage(imageUrl, holder.imageView, holder.progressBar, metrics.widthPixels / 3);

        return convertView;
    }

    public static class Holder {
        public ImageView imageView;
        public ProgressBar progressBar;

        public Holder(View view) {
            imageView   = (ImageView) view.findViewById(R.id.image_view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }
}
