package com.nemoeslovo.imagesearch.activity;

import android.widget.AbsListView;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class CustomScrollListener implements AbsListView.OnScrollListener {

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount == 0) {
            lastItemTotal = 0;
        }
        handleScrollEnd(firstVisibleItem, visibleItemCount, totalItemCount);
    }

    private int lastItemTotal = 0;
    private void handleScrollEnd(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount
                && lastItemTotal != totalItemCount
                && totalItemCount > 0) {

            if (scrollEndlistener != null) {
                scrollEndlistener.scrollEndReached();
                lastItemTotal = totalItemCount;
            }
        }
    }

    private ScrollEndListener scrollEndlistener;

    public void setScrollEndlistener(ScrollEndListener scrollEndlistener) {
        this.scrollEndlistener = scrollEndlistener;
    }

    public static interface ScrollEndListener {
        public void scrollEndReached();
    }
}