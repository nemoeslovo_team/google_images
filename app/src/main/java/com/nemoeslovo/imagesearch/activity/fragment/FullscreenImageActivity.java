package com.nemoeslovo.imagesearch.activity.fragment;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.activity.FullscreenImageFragment;

public class FullscreenImageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FullscreenImageFragment())
                    .commit();
        }
    }

}
