package com.nemoeslovo.imagesearch.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.activity.fragment.ImagesPreviewsFragment;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Fragment placeholderFragment = new ImagesPreviewsFragment();
            placeholderFragment.setRetainInstance(true);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, placeholderFragment)
                    .commit();
        }
    }


}
