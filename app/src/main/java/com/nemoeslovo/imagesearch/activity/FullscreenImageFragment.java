package com.nemoeslovo.imagesearch.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nemoeslovo.imagesearch.R;
import com.nemoeslovo.imagesearch.activity.adapter.ImagesAdapter;
import com.nemoeslovo.imagesearch.activity.fragment.ImagesPreviewsFragment;
import com.nemoeslovo.imagesearch.helper.UIHelper;

/**
* Created by danilakolesnikov on 09/02/14.
*/
public class FullscreenImageFragment extends Fragment {

    public FullscreenImageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fullscreen_image, container, false);

        ImagesAdapter.Holder holder = new ImagesAdapter.Holder(rootView);
        String url = getActivity().getIntent()
                                  .getStringExtra(ImagesPreviewsFragment.FULL_IMAGE_URL_TAG);

        if (url != null) {
            UIHelper.loadImage(url, holder.imageView, holder.progressBar);
        }

        return rootView;
    }
}
