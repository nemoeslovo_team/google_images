package com.nemoeslovo.imagesearch;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.nemoeslovo.imagesearch.business.imageloader.ImageLibrary;
import com.nemoeslovo.imagesearch.business.request.IRequestFactory;
import com.nemoeslovo.imagesearch.business.request.RequestFactory;

/**
 * Created by danilakolesnikov on 07/02/14.
 */
public class App extends Application {

    private static final String API_BASE_URL      = "https://ajax.googleapis.com/ajax/services/search";
    private static final long   ONE_MEGABYTE      = 1048576;

    private static    App            instance;
    private static IRequestFactory requestFactory;

    public App() {
        instance = this;
    }

    private static boolean isDebuggable() {
        return (0 != (App.getInstance().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static IRequestFactory getRequestFactory() {
        if (requestFactory == null) {
            requestFactory = new RequestFactory(API_BASE_URL);
        }
        return requestFactory;
    }

    private static ImageLibrary imageLibrary;
    public static ImageLibrary getImageLibrary() {
        if (imageLibrary == null) {
            imageLibrary = new ImageLibrary(App.getContext(), 20*ONE_MEGABYTE);
        }
        return imageLibrary;
    }
}
