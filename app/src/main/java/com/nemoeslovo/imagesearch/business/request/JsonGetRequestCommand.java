package com.nemoeslovo.imagesearch.business.request;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by danilakolesnikov on 11/02/14.
 */
public class JsonGetRequestCommand {

    private HttpGet    get;
    private HttpClient client;
    private boolean    canceled = false;

    public void cancel() {
        canceled = true;
        if (get != null)
            get.abort();
    }

    public JSONObject getResultJson(String url) throws IOException, JSONException {
        InputStream stream     = requestStream(url);
        JSONObject  jsonResult = streamToJSON(stream);
        return jsonResult;
    }

    protected InputStream requestStream(String url) throws IOException {
        canceled          = false;
        HttpGet get       = createGet(url);
        HttpClient client = createClient();
        HttpResponse response;
        try {
            response = client.execute(get);
            return response.getEntity().getContent();
        } catch (IOException e) {
            if (!canceled) {
                Log.e(getClass().getName(), "IOException", e);
            }
            throw e;
        }
    }

    protected HttpGet createGet(String fullUrl) {
        get = new HttpGet(fullUrl);
        get.setHeader("Content-Type", "application/json");
        return get;
    }

    protected HttpClient createClient() {

        client = JsonGetRequestCommand.createHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
        return client;
    }

    /*
    * fix problem with SSL on Froyo in unsequre way
    * of trusting all sertificates
    *
    * source http://stackoverflow.com/a/4837230/1290331
    *
    * TODO check certificates
    * */
    public static HttpClient createHttpClient() {

        SSLSocketFactory sf = null;
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }

        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
        HttpProtocolParams.setUseExpectContinue(params, true);


        SchemeRegistry schReg = new SchemeRegistry();
        schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schReg.register(new Scheme("https", sf, 443));
        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

        return new DefaultHttpClient(conMgr, params);
    }

    public static class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

    final protected JSONObject streamToJSON(InputStream stream) throws IOException, JSONException {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }

        Log.d(this.getClass().getName(), responseStrBuilder.toString());

        return new JSONObject(responseStrBuilder.toString());
    }

}
