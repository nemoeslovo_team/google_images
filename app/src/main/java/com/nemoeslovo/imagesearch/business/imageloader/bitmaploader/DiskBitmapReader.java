package com.nemoeslovo.imagesearch.business.imageloader.bitmaploader;

import android.graphics.Bitmap;

import com.nemoeslovo.imagesearch.cache.DiskCache;

import java.io.IOException;

/**
 * Created by danilakolesnikov on 13/02/14.
 */
public class DiskBitmapReader extends BitmapReader {

    private final DiskCache diskCache;

    public DiskBitmapReader(DiskCache diskCache, String url, int width, int height) {
        super(url, width, height);
        this.diskCache = diskCache;
    }

    @Override
    public Bitmap read() throws IOException {
        return decode(diskCache.get(getUrl()));
    }
}
