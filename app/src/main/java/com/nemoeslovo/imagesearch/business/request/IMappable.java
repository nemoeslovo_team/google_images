package com.nemoeslovo.imagesearch.business.request;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by danilakolesnikov on 11/02/14.
 */
public interface IMappable {

    public void mapWithJSONObject(JSONObject jsonObject) throws JSONException;

}
