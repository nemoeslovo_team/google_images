package com.nemoeslovo.imagesearch.business.imageloader.bitmaploader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;

import com.nemoeslovo.imagesearch.cache.DiskCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by danilakolesnikov on 17/02/14.
 */
public class TwoLevelCacheReader extends BitmapReader {

    private final LruCache<String, Bitmap> memoryCache;
    private final DiskCache diskCache;

    //TODO make an interface cache and facade above lru and disk cache
    public TwoLevelCacheReader(LruCache memoryCache, DiskCache diskCache, String url, int width, int height) {
        super(url, width, height);
        this.memoryCache = memoryCache;
        this.diskCache = diskCache;
    }

    @Override
    public Bitmap read() throws IOException {

        //look at memory cache
        Bitmap bitmap = memoryCache.get(getUrl());
        if (bitmap != null) {
            putInMemoryCache(bitmap);
            return bitmap;
        }

        //look at disk cache
        if (diskCache.contains(getUrl())) {
            File file = diskCache.getFile(getUrl());
            bitmap    = decodeFile(file);
            return bitmap;
        }

        File file = diskCache.getFile(getUrl());
        URL imageUrl = new URL(getUrl());
        HttpURLConnection conn     = (HttpURLConnection)imageUrl.openConnection();
        conn.setConnectTimeout(30000);
        conn.setReadTimeout(30000);
        conn.setInstanceFollowRedirects(true);
        InputStream inputStream = conn.getInputStream();
        diskCache.put(file, inputStream);
        bitmap = decodeFile(file);
        putInMemoryCache(bitmap);
        return bitmap;
    }

    private void putInMemoryCache(Bitmap bitmap) {
        if (bitmap != null) {
            memoryCache.put(getUrl(), bitmap);
        }
    }

    private Bitmap decodeFile(File f) throws FileNotFoundException {
        BitmapFactory.Options o = getJustInBoundsOptions();
        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

        int estimatedScale = estimateScale(o);

        return BitmapFactory.decodeStream( new FileInputStream(f)
                                         , null
                                         , getOptionsWithScale(estimatedScale));
    }
}
