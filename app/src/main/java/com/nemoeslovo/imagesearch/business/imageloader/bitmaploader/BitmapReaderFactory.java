package com.nemoeslovo.imagesearch.business.imageloader.bitmaploader;

import com.nemoeslovo.imagesearch.cache.DiskCache;

/**
 * Created by danilakolesnikov on 12/02/14.
 */
public class BitmapReaderFactory {

    public BitmapReader getNetworkBitmapReader(String urlString, int width, int height) {
        NetworkBitmapReader loader = new NetworkBitmapReader(urlString, width, height);
        return loader;
    }


    public BitmapReader getCacheBitmapReader(String urlString, DiskCache diskCache, int width, int height) {
        DiskBitmapReader reader = new DiskBitmapReader(diskCache, urlString, width, height);
        return reader;
    }
}
