package com.nemoeslovo.imagesearch.business.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nemoeslovo.imagesearch.business.imageloader.bitmaploader.BitmapReader;
import com.nemoeslovo.imagesearch.business.imageloader.bitmaploader.TwoLevelCacheReader;
import com.nemoeslovo.imagesearch.cache.DiskCache;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by danilakolesnikov on 12/02/14.
 */
public class ImageLibrary {

    private static final int CORE_POOL_SIZE    = 5;
    private static final int MAXIMUM_POOL_SIZE = 128;
    private static final int KEEP_ALIVE_TIME   = 1;
    public  static final int REQUEST_COMPLITED = 100;


    private final Map<ImageView, AsyncBitmapLoader> allJobs;
    private final ThreadPoolExecutor mExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE,
            MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS,
            new PriorityBlockingQueue<Runnable>());

    private final Context                 context;

    private final LruCache<String,Bitmap> memoryCache;
    private final DiskCache               diskCache;

    public ImageLibrary(Context context, long diskCacheSize) {
        this.context     = context;
        allJobs          = Collections.synchronizedMap(new HashMap<ImageView, AsyncBitmapLoader>());
        memoryCache      = new LruCache<String, Bitmap>((int) diskCacheSize);

        File diskCacheDir = new File(context.getCacheDir(), "imagediskcache");
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdir();
        }

        diskCache = new DiskCache(diskCacheDir, diskCacheSize);
    }

    public void load(final String urlString, final ImageView imageView, int width, int height) {

        if (width == 0 || height == 0) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            width = metrics.widthPixels;
            height = metrics.heightPixels;
        }

        if (allJobs.containsKey(imageView)) {
            cancel(imageView);
        }

        Bitmap bitmap = memoryCache.get(urlString);
        if (bitmap != null) {
            fillImageView(imageView, bitmap, width, height);
            return;
        } else {
            imageView.setImageResource(android.R.color.black);
        }

        BitmapReader bitmapReader = new TwoLevelCacheReader( memoryCache
                                                           , diskCache
                                                           , urlString
                                                           , width
                                                           , height);

        AsyncBitmapLoader loader = new AsyncBitmapLoader( this
                                                        , HANDLER
                                                        , imageView
                                                        , bitmapReader);

        allJobs.put(imageView, loader);
        mExecutor.execute(loader);
    }

    private void cancel(ImageView imageView) {
        AsyncBitmapLoader runnable = allJobs.get(imageView);
        runnable.setCancel(true);
        mExecutor.remove(runnable);
        allJobs.remove(imageView);
    }

    private void handleRequestComplite(AsyncBitmapLoader loader) {
        if (!allJobs.containsKey(loader.getImageView())) {
            return;
        }
        loader.getImageLibrary().allJobs.remove(loader.getImageView());
        loader.getImageLibrary().fillImageView(loader);
    }

    private void fillImageView(ImageView imageView, Bitmap bitmap, int width, int height) {
        if (bitmap != null) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
            imageView.setImageBitmap(bitmap);
        } else {
            //TODO fill with placeholder
        }
    }

    private void fillImageView(AsyncBitmapLoader loader) {
        int width  = loader.getReader().getDestinationWidth();
        int height = loader.getReader().getDestinationHeight();
        fillImageView(loader.getImageView(), loader.getBitmap(), width, height);
    }

    //============== handler ====================//
    private static final Handler HANDLER = new Handler(Looper.getMainLooper()) {

        @Override public void handleMessage(Message msg) {
            switch (msg.what) {
                case REQUEST_COMPLITED: {
                    AsyncBitmapLoader loader = (AsyncBitmapLoader) msg.obj;
                    loader.getImageLibrary().handleRequestComplite(loader);
                    break;
                }
                default:
                    throw new AssertionError("Unknown handler message received: " + msg.what);
            }
        }
    };

}
