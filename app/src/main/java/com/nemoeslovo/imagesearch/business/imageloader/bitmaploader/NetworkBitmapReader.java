package com.nemoeslovo.imagesearch.business.imageloader.bitmaploader;

import android.graphics.Bitmap;

import com.nemoeslovo.imagesearch.helper.StreamUtil;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by danilakolesnikov on 12/02/14.
 */
public class NetworkBitmapReader extends BitmapReader {

    public NetworkBitmapReader(String url, int width, int height) {
        super(url, width, height);
    }

    @Override
    public Bitmap read() throws IOException {
        final URL url                = new URL(getUrl());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        return decode(StreamUtil.convertStreamToByteArray(connection.getInputStream()));
    }
}
