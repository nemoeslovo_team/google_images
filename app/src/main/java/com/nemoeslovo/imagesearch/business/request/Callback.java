package com.nemoeslovo.imagesearch.business.request;

/**
 * Created by danilakolesnikov on 12/02/14.
 */
public interface Callback<T extends IMappable> {

    public void success(T result);
    public void failure(boolean isConnectionProblem, Exception exception);

}
