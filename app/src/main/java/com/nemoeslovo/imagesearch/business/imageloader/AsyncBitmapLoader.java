package com.nemoeslovo.imagesearch.business.imageloader;

import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;

import com.nemoeslovo.imagesearch.business.imageloader.bitmaploader.BitmapReader;

import java.io.IOException;

/**
* Created by danilakolesnikov on 12/02/14.
*/
public class AsyncBitmapLoader implements Runnable, Comparable<AsyncBitmapLoader> {

    final private BitmapReader reader;
    private ImageLibrary imageLibrary;
    private Handler            handler;
    private final ImageView    imageView;
    private Bitmap bitmap;
    private boolean cancel;


    public AsyncBitmapLoader(ImageLibrary imageLibrary, Handler handler, ImageView imageView, BitmapReader reader) {
        this.imageLibrary = imageLibrary;
        this.handler = handler;
        this.imageView = imageView;
        if (reader == null) {
            throw new NullPointerException("reader must be not null");
        }
        this.reader   = reader;
    }

    @Override
    public void run() {
        bitmap = null;
        try {
            bitmap = reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bitmap != null && !isCancel()) {
            handler.sendMessage(handler.obtainMessage(ImageLibrary.REQUEST_COMPLITED, this));
        }

    }

    public ImageView getImageView() {
        return imageView;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public int compareTo(AsyncBitmapLoader asyncBitmapLoader) {
        return imageView.getId() == asyncBitmapLoader.getImageView().getId() ? 0 : 1;
    }

    public ImageLibrary getImageLibrary() {
        return imageLibrary;
    }

    public BitmapReader getReader() {
        return reader;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public boolean isCancel() {
        return cancel;
    }
}
