package com.nemoeslovo.imagesearch.business.imageloader.bitmaploader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;

/**
 * Created by danilakolesnikov on 12/02/14.
 */
public abstract class BitmapReader {

    private String url;
    private final int width;
    private final int height;
    private byte[] bytes;

    public BitmapReader(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }

    // TODO bottleneck converting streams to byte arrays causes memory leaks.
    // rewrite with using streams.
    protected Bitmap decode(byte[] bytes) throws IOException {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        this.bytes = bytes;

        // decode image size and estimate proper scale
        final BitmapFactory.Options o = getJustInBoundsOptions();
        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, o);
        int scale = estimateScale(o);

        //getting now scale factor for properly image loading
        final Bitmap scaledImage = BitmapFactory
                .decodeByteArray(bytes, 0, bytes.length, getOptionsWithScale(scale));

        return scaledImage;
    }

    protected BitmapFactory.Options getJustInBoundsOptions() {
        final BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        return o;
    }

    protected BitmapFactory.Options getOptionsWithScale(int scale) {
        final BitmapFactory.Options optionsWithScale = new BitmapFactory.Options();
        optionsWithScale.inSampleSize = scale;
        return optionsWithScale;
    }

    protected int estimateScale(BitmapFactory.Options o) {
        int estimatedWidth  = o.outWidth;
        int estimatedHeight = o.outHeight;

        int scale = 1;
        while (  estimatedWidth  / 2 > getDestinationWidth()
                || estimatedHeight / 2 > getDestinationHeight()) {

            estimatedWidth  /= 2;
            estimatedHeight /= 2;
            scale           *= 2;
        }
        return scale;
    }

    public abstract Bitmap read() throws IOException;

    public String getUrl() {
        return url;
    }

    public int getDestinationHeight() {
        return height;
    }

    public int getDestinationWidth() {
        return width;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
