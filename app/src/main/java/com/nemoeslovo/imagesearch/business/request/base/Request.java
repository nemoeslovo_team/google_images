package com.nemoeslovo.imagesearch.business.request.base;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by danilakolesnikov on 11/02/14.
 */
public class Request {

    private final String method;
    private final String url;

    public Request(String method, String url) {
        this.method = method;
        this.url = url;
    }


    //=========== Getters =========//
    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    //============ Builder class =======//

    public static class Builder {
        private String        baseUrl;
        private String        method;
        private StringBuilder queryParameters;

        public Builder(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public Builder setMethod(String method) {
            if (!method.startsWith("/")) {
                throw new IllegalArgumentException("method should start with \\/");
            }

            this.method = method;
            return this;
        }

        public Builder addParameter(ParameterType type, String key, Object value) {
            if (value == null) return this;
            switch (type) {
                case GET:
                    addGetParameter(key, value);
                    break;
                default:
                    break;

            }
            return this;
        }

        public Request build() {
            StringBuilder fullUrl = new StringBuilder(baseUrl).append(method)
                                                              .append(queryParameters.toString());
            return new Request(method, fullUrl.toString());
        }

        private void addGetParameter(String key, Object value) {
            String valueStr = null;
            try {
                valueStr = URLEncoder.encode(String.valueOf(value.toString()), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(this.getClass().getName(), "Can't encode query param");
            }


            if (queryParameters == null) {
                queryParameters = new StringBuilder();
            }

            queryParameters.append(queryParameters.length() > 0 ? "&" : "?");
            queryParameters.append(key).append("=").append(valueStr);
        }

    }

}
