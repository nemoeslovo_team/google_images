package com.nemoeslovo.imagesearch.business.request;

import com.nemoeslovo.imagesearch.business.request.base.Request;

/**
 * Created by danilakolesnikov on 11/02/14.
 */
public interface IRequestFactory {

    public Request getImageSearchRequest(Integer start, String query);

}
