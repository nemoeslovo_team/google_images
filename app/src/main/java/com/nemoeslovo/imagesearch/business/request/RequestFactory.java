package com.nemoeslovo.imagesearch.business.request;

import com.nemoeslovo.imagesearch.business.request.base.ParameterType;
import com.nemoeslovo.imagesearch.business.request.base.Request;

/**
 * Created by danilakolesnikov on 11/02/14.
 */

public class RequestFactory implements IRequestFactory {

    public  static final String IMAGES_SEARCH_METHOD = "/images";
    private        final String apiBaseUrl;

    public RequestFactory(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    public Request getImageSearchRequest(Integer start, String query) {
        Request.Builder builder = getDefaultRequestBuilder()
                                  .setMethod(IMAGES_SEARCH_METHOD)
                                  .addParameter(ParameterType.GET, "start", start)
                                  .addParameter(ParameterType.GET, "q", query);

        return builder.build();
    }

    private Request.Builder getDefaultRequestBuilder() {
        return new Request.Builder(apiBaseUrl)
                          .addParameter(ParameterType.GET, "v", "1.0");
    }

}