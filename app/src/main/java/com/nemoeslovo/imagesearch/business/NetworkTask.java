package com.nemoeslovo.imagesearch.business;

import android.os.AsyncTask;
import android.util.Log;

import com.nemoeslovo.imagesearch.App;
import com.nemoeslovo.imagesearch.business.request.Callback;
import com.nemoeslovo.imagesearch.business.request.IMappable;
import com.nemoeslovo.imagesearch.business.request.JsonGetRequestCommand;
import com.nemoeslovo.imagesearch.business.request.base.Request;
import com.nemoeslovo.imagesearch.helper.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by danilakolesnikov on 11/02/14.
 */
public class NetworkTask<Result extends IMappable> extends AsyncTask<Void, Void, Result> {

    private boolean isComplete = false;
    private boolean isAborted  = false;

    public boolean isComplete() {
        return isComplete;
    }

    public boolean isAborted() {
        return isAborted;
    }

    private final Request  request;
    private final Callback callback;
    private final Class    resultClass;

    public NetworkTask(final Request request, final Callback callbackWrapper, Class resultClass) {
        this.request         = request;
        this.callback = callbackWrapper;

        //TODO get class from generic
        //but now try to keep it simple
        this.resultClass     = resultClass;
    }

    public void execute() {
        execute(null, null);
    }


    protected Result doNetworkAction() throws IOException, JSONException, IllegalAccessException, InstantiationException {

        JsonGetRequestCommand command    = new JsonGetRequestCommand();
        JSONObject            jsonObject = command.getResultJson(request.getUrl());
        Result                result     = (Result)resultClass.newInstance();

        if (result != null) {
            result.mapWithJSONObject(jsonObject);
        }

        //TODO: implement log level
        if (jsonObject != null) {
            Log.d(this.getClass().getName(), jsonObject.toString());
        }
        return result;
    }

    IOException            ioException;
    JSONException          jsonException;
    IllegalAccessException illegalAccessException;
    InstantiationException instantiationException;

    @Override
    protected Result doInBackground(Void... voids) {
        try {
            return doNetworkAction();
        } catch (IOException e) {
            ioException = e;
        } catch (JSONException e) {
            jsonException = e;
        } catch (IllegalAccessException e) {
            illegalAccessException = e;
        } catch (InstantiationException e) {
            instantiationException = e;
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isComplete = false;
        isAborted  = false;
        boolean hasNetworkConnection = NetworkUtil.hasInternetAccess(App.getContext());

        if (!hasNetworkConnection) {
            callback.failure(!hasNetworkConnection, null);
        }
    }


    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);

        isComplete = true;
        if (isCancelled() || isAborted()) {
            return;
        }

        // Failure sending
        if (jsonException != null) {
            callback.failure(false, jsonException);
        } else if (ioException != null) {
            callback.failure(false, ioException);
        } else if (illegalAccessException != null) {
            callback.failure(false, illegalAccessException);
        } else if (instantiationException != null) {
            callback.failure(false, instantiationException);
        }

        // Success case
        else {
            callback.success(result);
        }


    }
}
