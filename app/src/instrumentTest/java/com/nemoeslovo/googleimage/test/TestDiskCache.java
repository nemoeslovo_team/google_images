package com.nemoeslovo.googleimage.test;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.nemoeslovo.imagesearch.cache.DiskCache;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by danilakolesnikov on 13/02/14.
 */
public class TestDiskCache extends AndroidTestCase {

    private File standardCacheDir;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        File standardCacheDir = getContext().getCacheDir();
        this.standardCacheDir = new File(standardCacheDir, "diskCache");
        this.standardCacheDir.mkdir();
    }

    @MediumTest
    public void testPutGet() throws IOException {
        DiskCache diskCache = new DiskCache(standardCacheDir, 100);
        putTestSmallFileWithKey("yoyo", diskCache);

        byte[] readed = diskCache.get("yoyo");
        assertTrue(Arrays.equals(smallBytesArray, readed));
    }


    public void testUsedSpaceWork() {
        DiskCache diskCache = new DiskCache(standardCacheDir, 100);
        diskCache.clear();
        putTestSmallFileWithKey("test", diskCache);

        assertEquals(smallBytesArray.length, diskCache.getUsedSpace());
    }

    @MediumTest
    public void testClearCache() {
        DiskCache diskCache = new DiskCache(standardCacheDir, 100);
        putTestSmallFileWithKey("test0", diskCache);
        putTestSmallFileWithKey("test1", diskCache);
        putTestSmallFileWithKey("test2", diskCache);
        putTestSmallFileWithKey("test3", diskCache);

        assertTrue(diskCache.clear());
        assertEquals(0, diskCache.getUsedSpace());
    }

    @MediumTest
    public void testTrimToSize() {
        int filesCount = 10;

        File file = new File(getContext().getCacheDir(), "yo");
        if (!file.exists()) {
            file.mkdir();
        }


        DiskCache diskCache = new DiskCache(file, smallBytesArray.length * filesCount);
        diskCache.clear();

        for (int i = 0; i < filesCount - 1; i++) {
            putTestSmallFileWithKey("test" + i, diskCache);
        }
        assertEquals(smallBytesArray.length * (filesCount - 1), diskCache.getUsedSpace());
        putTestSmallFileWithKey("_test", diskCache);

        assertEquals(smallBytesArray.length * filesCount, diskCache.getUsedSpace());

        for (int i = 0; i < filesCount*10; i++) {
            putTestSmallFileWithKey("testtest" + i, diskCache);
        }

        Log.d(this.getClass().getName(), "used space after very full" + diskCache.getUsedSpace());

        Long estimatedCount = Long.valueOf(filesCount * 10 + filesCount + 1);
        assertNotSame(estimatedCount, diskCache.getUsedSpace());
    }

    @MediumTest
    public void testRecreateCache() {
        DiskCache cache = new DiskCache(standardCacheDir, 1024);
        cache.clear();
        putTestSmallFileWithKey("tessst", cache);

        DiskCache cache1 = new DiskCache(standardCacheDir, 1024);
        assertTrue(cache1.contains("tessst"));
        assertEquals(smallBytesArray.length, cache1.getUsedSpace());
    }


    private static final byte[] smallBytesArray = {21, 21, 21, 53, 54, 34, 23, 53};
    private void putTestSmallFileWithKey(String key, DiskCache diskCache) {
        try {
            diskCache.put(key, smallBytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
